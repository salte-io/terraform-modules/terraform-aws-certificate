# Terraform Module: RDS

> Simplified ACM Certificate creation within Salte!

## Table of Contents

* [Dependencies](#dependencies)
* [Usage](#usage)
  * [Input Variables](#input-variables)
  * [Output Variables](#output-variables)
* [Author Information](#author-information)

## Dependencies

This module depends on a correctly configured [AWS Provider](https://www.terraform.io/docs/providers/aws/index.html) in your Terraform codebase.

## Usage

```tf
data "aws_route53_zone" "default" {
  name = "quillie.net."
}

module "certificate" {
  source  = "git::https://gitlab.com/salte-io/terraform-modules/terraform-aws-certificate.git?ref=1.0.5"

  zone_id = "${data.aws_route53_zone.default.zone_id}"
  domain_name = "api.alpha.quillie.net"
}
```

Then, load the module using `terraform init`.

### Input Variables

Available variables are listed below, along with their default values:

| **Variable** | **Description** | **Required?** |
| -------- | ----------- | ------- |
| `zone_id` | The zone to create a certificate for. | **Yes** |
| `domain_name` | The domain name to create a certificate for. | **Yes** |
| `subject_alternative_names` | A list of other valid domains associated to this certificate. | No |

### Output Variables

Available output variables are listed below:

| **Variable** | **Description** |
| -------- | ----------- |
| `arn` | The generated certificate arn. |

## Author Information

This module is currently maintained by the individuals listed below.

* [Nick Woodward](https://github.com/nick-woodward)
