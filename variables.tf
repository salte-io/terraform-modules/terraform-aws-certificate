variable "zone_id" {
  type        = string
  description = "The zone to create a certificate for."
}

variable "domain" {
  type        = string
  description = "The domain name to create a certificate for."
}

variable "subject_alternative_names" {
  type        = list(string)
  description = "A list of other valid domains associated to this certificate."
  default     = []
}
